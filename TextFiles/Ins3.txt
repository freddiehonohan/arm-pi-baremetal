[3:03 PM] 2 blockchainz: tell me when you read this
[3:03 PM] spookthecucks: i'm here, just read
[3:03 PM] 2 blockchainz: okay, so hear me out on this
[3:03 PM] 2 blockchainz: there are definitely files in the blockchain
[3:03 PM] 2 blockchainz: BUT
[3:04 PM] 2 blockchainz: what if we've been told so many times to look for headers because they use them as flags for other data
[3:04 PM] 2 blockchainz: that transaction (b2d9c88b629efe61fa63240c79cd05031d131fa02edddef01b2e94d414bc5b) i found a long long time ago, whenever it was blasted on the chans
[3:04 PM] 2 blockchainz: it has what looks like a tar.gz header
[3:04 PM] 2 blockchainz: doesn't turn into an archive, but what if the string after it is all that matters?
[3:04 PM] 2 blockchainz: what if we're supposed to flag these because that's how they get people to find the information
[3:04 PM] 2 blockchainz: when it's not a file at all
[3:04 PM] 2 blockchainz: or at least in some cases
[3:04 PM] 2 blockchainz: you follow?
[3:05 PM] 2 blockchainz: i'm just brainstorming but i think it makes sense
