/*
SHA1 tests by Philip Woolford <woolford.philip@gmail.com>
100% Public Domain
*/

#include "sha1.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

// xxd --include ../GPG/extracted/gnupg-2.0.19/g10/testAAA.txt.gpg 
unsigned char ___GPG_extracted_gnupg_2_0_19_g10_testAAA_txt_gpg[] = {
  0x8c, 0x0d, 0x04, 0x03, 0x03, 0x02, 0xce, 0xc2, 0x52, 0x33, 0x34, 0xd2,
  0xae, 0x99, 0xff, 0xc9, 0x25, 0x40, 0xa2, 0x9b, 0x07, 0xfb, 0x88, 0xa5,
  0x3d, 0x70, 0x27, 0x7c, 0xfa, 0xf0, 0x4a, 0xe4, 0xd1, 0xd8, 0x93, 0x34,
  0x60, 0x27, 0x18, 0x5d, 0xa1, 0x57, 0xa8, 0x35, 0xf6, 0x6f, 0xf4, 0xec,
  0xa8, 0x2f, 0xb1, 0x0c, 0x41, 0x08
};
// 0x40, 0xa2, 0x9b, 0x07, 0xfb, 0x88, 0xa5, 0x3d, 0x70, 0x27, 0x7c, 0xfa, 0xf0, 0x4a, 0xe4, 0xd1, 0xd8, 0x93, 0x34, 0x60, 0x27, 0x18, 0x5d, 0xa1, 0x57             , 0xa8, 0x35, 0xf6, 0x6f, 0xf4, 0xec, 0xa8, 0x2f, 0xb1, 0x0c, 0x41, 0x08
// 220 / 4 = 55
// 55 - 37 = 18

// , 0xa8, 0x35, 0xf6, 0x6f, 0xf4, 0xec, 0xa8, 0x2f, 0xb1, 0x0c, 0x41, 0x08

/*./gpg2 --list-packets testAAA.txt.gpg 
:symkey enc packet: version 4, cipher 3, s2k 3, hash 2
        salt cec2523334d2ae99, count 65011712 (255)
:encrypted data packet:
        length: 37
:compressed packet: algo=1
:literal data packet:
        mode b (62), created 1657300859, name="testAAA.txt",
        raw data: 129 bytes
*/

// **** gpg: session key: `3:032D070DAD7BF2157918C4649B45AF98'
// **** '3:' pertains to cipher ****

unsigned int ___GPG_extracted_gnupg_2_0_19_g10_testAAA_txt_gpg_len = 54;

// echo password | xxd --include
// 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x0a
/*
pgpdump g10/testAAA.txt.gpg pgpdump g10/testAAA.txt.gpg 
Old: Symmetric-Key Encrypted Session Key Packet(tag 3)(13 bytes)
        New version(4)
        Sym alg - CAST5(sym 3)
        Iterated and salted string-to-key(s2k 3):
                Hash alg - SHA1(hash 2)
                Salt - ce c2 52 33 34 d2 ae 99 
                Count - 65011712(coded count 255)
New: Symmetrically Encrypted Data Packet(tag 9)(37 bytes)
        Encrypted data [sym alg is specified in sym-key encrypted session key]
*/
/* Byte1: gpg packet tag
Byte 2: packet length
3: version tag
4: algo
5: s2k
6: hash algo
7 - 15: 8 byte salt
16: count (hash algo iterations)
17: gpg packet tag
18: packet length
*/
/* Test Vector 1 */

/*

3.7.1.3.  Iterated and Salted S2K

   This includes both a salt and an octet count.  The salt is combined
   with the passphrase and the resulting value is hashed repeatedly.
   This further increases the amount of work an attacker must do to try
   dictionary attacks.

       Octet  0:        0x03
       Octet  1:        hash algorithm
       Octets 2-9:      8-octet salt value
       Octet  10:       count, a one-octet, coded value



   The count is coded into a one-octet number using the following
   formula:

       #define EXPBIAS 6
           count = ((Int32)16 + (c & 15)) << ((c >> 4) + EXPBIAS);

   The above formula is in C, where "Int32" is a type for a 32-bit
   integer, and the variable "c" is the coded count, Octet 10.

   Iterated-Salted S2K hashes the passphrase and salt data multiple
   times.  The total number of octets to be hashed is specified in the
   encoded count in the S2K specifier.  
   ********************************************************************
   Note that the resulting count
   value is an octet count of how many octets will be hashed, not an
   iteration count.
   **********************************************************************
   Initially, one or more hash contexts are set up as with the other S2K
   algorithms, depending on how many octets of key data are needed.
   Then the salt, followed by the passphrase data, is repeatedly hashed
   until the number of octets specified by the octet count has been
   hashed.  The one exception is that if the octet count is less than
   the size of the salt plus passphrase, the full salt plus passphrase
   will be hashed even though that is greater than the octet count.
   After the hashing is done, the data is unloaded from the hash
   context(s) as with the other S2K algorithms.*/

   // https://raysnotebook.info/computing/crypto-gnupg-s2k.html
   // The passphrase is "abc", and the count is 1,408. Thus the salted passphrase appears 128 times: (8+3)×128 = 1,408. 

#define EXPBIAS 6

void testS2k(
    void
)
{
  // char const string[] = "abc";
  char const passphrase[] = "password";
  unsigned char salt [] = {0xce, 0xc2, 0x52, 0x33, 0x34, 0xd2, 0xae, 0x99};
  // char const expect[] = "a9993e364706816aba3e25717850c26c9cd0d89d";
  char const expected[] = "032D070DAD7BF2157918C4649B45AF98";

  int passphraseLength = sizeof(passphrase);
  int saltLength = sizeof(salt);
  printf("passphrase: %i\n",passphraseLength);
  printf("salt: %i\n",saltLength);
  printf("passphrase+salt->length: %i\n",passphraseLength+saltLength);

unsigned char saltPlusPassphrase[sizeof(salt)+strlen(passphrase)];
memcpy(saltPlusPassphrase,salt,sizeof(salt));
memcpy(saltPlusPassphrase+sizeof(salt),passphrase,sizeof(passphrase));

  printf("%.*s\n", (int) sizeof (saltPlusPassphrase), saltPlusPassphrase);
  printf("passphrase+salt: %s\n",saltPlusPassphrase);



  int c = 0xff; // Swap this out for actual byte count
  int octetExpansionCount = ((int32_t)16 + (c & 15)) << ((c >> 4) + EXPBIAS);

  printf("octetExpansionCount: %i\n",octetExpansionCount);


  int totalLen = sizeof(saltPlusPassphrase);
  printf("saltPlusPassphraseLength: %i\n",totalLen);

  /*int repeatCount = octetExpansionCount/totalLen;

  printf("repeatCount: %i\n",repeatCount);*/

  // Now we multiply the total length by the repeat count
  // And loop copying a single byte at a time, wrapping around the end to the start

  int byteIndex = 0;
  // unsigned char saltPlusPassphraseExpanded[octetExpansionCount];
  unsigned char* b = (unsigned char*)malloc(octetExpansionCount);
  for(int i = 0;i<octetExpansionCount;i++){

    memcpy(b+(i*sizeof(unsigned char)),&saltPlusPassphrase[byteIndex],sizeof(unsigned char));
    // memcpy(saltPlusPassphraseExpanded+(i*sizeof(unsigned char)),&saltPlusPassphrase[byteIndex],sizeof(unsigned char));

    byteIndex++;
    if(byteIndex==totalLen)// Wrap around end
      byteIndex = 0;
  }

  printf("passphrase+salt EXPANDED: %li\n",strlen((char*)b));

  char result[21];
  /* calculate hash */
  SHA1( result,(char*)b, strlen((char*)b) );

  char hexresult[41];
  size_t offset;
  
  /* format the hash for comparison */
  for( offset = 0; offset < 20; offset++) {
    sprintf( ( hexresult + (2*offset)), "%02x", result[offset]&0xff);
  }

  printf("Hex result: %s\n",hexresult);
  printf("Expected: %s\n", expected);
}

int main(
    void
)
{
    testS2k();
}
