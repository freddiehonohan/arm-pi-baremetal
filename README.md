# ARM-PI-BareMetal

A copy of the excellent Cambridge [Operating Systems course](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/index.html), with a full copy of the [downloads](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/downloads/).

Notes of the lessons have been taken in [NOTES.md](./NOTES.md).

# Text to UART
git clone github.com/BrianSidebotham/arm-tutorial-rpi.git
$ compiler/get_compiler.sh

# Install latest qemu
$ apt install ninja-build libglib2.0-dev libpixman-1-dev tigervnc build-essential cmake gdb-multiarch

# Install and Configure TigerVNC server on Ubuntu 18.04 18.10
sudo apt install tigervnc-standalone-server tigervnc-xorg-extension tigervnc-viewer

git clone https://gitlab.com/qemu-project/qemu.git
cd qemu && \ 
git submodule init && \ 
git submodule update --recursive && \ 
scripts/git-submodule.sh update ui/keycodemapdb meson tests/fp/berkeley-testfloat-3 tests/fp/berkeley-softfloat-3 dtc slirp && \
./configure --with-git-submodules=validate && \
make -j16


qemu-system-aarch64 -m 1G -serial null -serial mon:stdio -nographic -M raspi3b -kernel build/kernel.armc-014.rpi3bp.img
qemu-system-aarch64 -m 1G -M raspi3b -cpu cortex-a53 -kernel build/kernel.armc-014.rpi3bp.img -nographic -serial null -chardev stdio,id=uart1 -serial chardev:uart1 -monitor none

https://github.com/s-matyukevich/raspberry-pi-os/issues/8

https://documentation.suse.com/sles/12-SP5/html/SLES-all/cha-qemu-monitor.html

https://fedoraproject.org/wiki/Architectures/AArch64/Booting_a_32-Bit_QEMU_image


Also, we're using the mini uart which is not the default output for qemu, so we need to tweak the options a little bit to get the output of the UART we're using the tutorials. My qemu-system-arm version doesn't support raspi3b, so here I show the full emulation command I can use to get the graphics and UART output:

$ ./build.sh rpi1

$ qemu-system-arm -M raspi1ap -kernel kernel.armc-014.rpi1 -serial null -serial mon:stdio

If you want to get rid of the graphical output then just add the -nographic option to the qemu command line.

Convert binary file to C include:
xxd --include test.txt 
unsigned char test_txt[] = {
  0x54, 0x68, 0x69, 0x73, 0x20, 0x69, 0x73, 0x20, 0x61, 0x20, 0x74, 0x65,
  0x78, 0x74, 0x20, 0x66, 0x69, 0x6c, 0x65, 0x20, 0x74, 0x6f, 0x20, 0x63,
  0x6f, 0x6e, 0x76, 0x65, 0x72, 0x74, 0x20, 0x74, 0x6f, 0x20, 0x68, 0x65,
  0x78, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x62, 0x65, 0x20, 0x73, 0x74, 0x6f,
  0x72, 0x65, 0x64, 0x20, 0x69, 0x6e, 0x20, 0x61, 0x20, 0x43, 0x20, 0x66,
  0x69, 0x6c, 0x65, 0x20, 0x74, 0x6f, 0x20, 0x62, 0x65, 0x20, 0x70, 0x72,
  0x69, 0x6e, 0x74, 0x65, 0x64, 0x20, 0x74, 0x6f, 0x20, 0x73, 0x63, 0x72,
  0x65, 0x65, 0x6e, 0x2e, 0x0a
};
unsigned int test_txt_len = 89;

# Building GPG (Ubuntu)
sudo apt-get install texinfo
gnupg-2.0.19
 	gnupg-2.0.19.tar.bz2	2012-03-27	3M
https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.0.19.tar.bz2

***   ftp://ftp.gnupg.org/gcrypt/libgpg-error
*** (at least version 1.7 is required.)

https://gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.10.tar.gz

***   ftp://ftp.gnupg.org/gcrypt/libgcrypt/
*** (at least version 1.4.0 using API 1 is required.)

***   ftp://ftp.gnupg.org/gcrypt/libassuan/
*** (at least version 2.0.0 (API 2) is required).

***   ftp://ftp.gnupg.org/gcrypt/libksba/
*** (at least version 1.0.7 using API 1 is required).

***   ftp://ftp.gnu.org/gnu/pth/
*** On a Debian GNU/Linux system you can install it using
***   apt-get install libpth-dev


Install libgpg-error first:
./autogen
./configure --enable-maintainer-mode && make && sudo make install 


Then do libgcrypt (1.4.0)
./autogen
env CFLAGS="-std=gnu89" ./configure --enable-maintainer-mode && make #(fails but only on docs so the main logic "installs")

Then do libassuan
./autogen
automake --add-missing
./configure --enable-maintainer-mode && make

Then libksba
./autogen
./configure --enable-maintainer-mode && make

Then gnupg

/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/sign.c:820: undefined reference to `gcry_md_start_debug'
/usr/bin/ld: sign.o: in function `clearsign_file':
/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/sign.c:1169: undefined reference to `gcry_md_start_debug'
/usr/bin/ld: sign.o: in function `sign_symencrypt_file':
/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/sign.c:1291: undefined reference to `gcry_md_start_debug'
/usr/bin/ld: mainproc.o: in function `proc_plaintext':
/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/mainproc.c:702: undefined reference to `gcry_md_start_debug'
/usr/bin/ld: /home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/mainproc.c:704: undefined reference to `gcry_md_start_debug'
/usr/bin/ld: mainproc.o:/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10/mainproc.c:2141: more undefined references to `gcry_md_start_debug' follow
collect2: error: ld returned 1 exit status
make[2]: *** [Makefile:908: gpg2] Error 1
make[2]: Leaving directory '/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19/g10'
make[1]: *** [Makefile:592: all-recursive] Error 1
make[1]: Leaving directory '/home/ian/arm-pi-baremetal/GPG/extracted/gnupg-2.0.19'


 * The new function gcry_md_debug should be used instead of the
   gcry_md_start_debug and gcry_md_stop_debug macros.

   Find and replace 'gcry_md_debug' with 'gcry_md_debug'

   Function: void gcry_md_debug ( gcry_md_hd_t h, const char *suffix 


Go into the libgcrypt folder and run make install then sudo ldconfig 



automake --add-missing
./configure --sysconfdir=/etc --enable-maintainer-mode --disable-scdaemon --disable-doc && make

DEADBEEFBABECAFE0100111001110010DEADBEEFBABECAFE0100111001110010DEADBEEFBABECAFE0100111001110010DEADBEEFBABECAFE0100111001110010
AAAABBBBCCCCDDDDEEEEFFFF0000111122223333444455556666777788889999AAAABBBBCCCCDDDDEEEEFFFF0000111122223333444455556666777788889999
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

Encrypt:
./g10/gpg2 -c --batch --passphrase password testAAA.txt

Decrypt:
./g10/gpg2 -d --batch --passphrase password testAAA.txt.gpg

# Cast 5
https://en.wikipedia.org/wiki/CAST-128
Successor: Cast 6 (256)


pgpdump GpgCast5/testAAA.txt.gpg 

typedef enum 
  {
    PKT_SYMKEY_ENC    = 3,  /* Session key packet. */
    PKT_ENCRYPTED     = 9,  /* Conventional encrypted data. */
    PKT_COMPRESSED    = 8,  /* Compressed data packet. */
    PKT_PLAINTEXT     = 11, /* Literal data packet. */
  } 
pkttype_t;

Old: Symmetric-Key Encrypted Session Key Packet(tag 3)(13 bytes)
        New version(4)
        Sym alg - CAST5(sym 3)
        Iterated and salted string-to-key(s2k 3):
                Hash alg - SHA1(hash 2)
                Salt - 43 b7 a2 56 d8 47 6b 18 
                Count - 65536(coded count 96)


https://www.rfc-editor.org/rfc/rfc4880#section-3.7.1

3.7.1.  String-to-Key (S2K) Specifier Types

   There are three types of S2K specifiers currently supported, and
   some reserved values:

       ID          S2K Type
       --          --------
       0           Simple S2K
       1           Salted S2K
       2           Reserved value
       3           Iterated and Salted S2K
       100 to 110  Private/Experimental S2K

   These are described in Sections 3.7.1.1 - 3.7.1.3.

3.7.1.1.  Simple S2K

   This directly hashes the string to produce the key data.  See below
   for how this hashing is done.

       Octet 0:        0x00
       Octet 1:        hash algorithm

   Simple S2K hashes the passphrase to produce the session key.  The
   manner in which this is done depends on the size of the session key
   (which will depend on the cipher used) and the size of the hash

   algorithm's output.  If the hash size is greater than the session key
   size, the high-order (leftmost) octets of the hash are used as the
   key.

   If the hash size is less than the key size, multiple instances of the
   hash context are created -- enough to produce the required key data.
   These instances are preloaded with 0, 1, 2, ... octets of zeros (that
   is to say, the first instance has no preloading, the second gets
   preloaded with 1 octet of zero, the third is preloaded with two
   octets of zeros, and so forth).

   As the data is hashed, it is given independently to each hash
   context.  Since the contexts have been initialized differently, they
   will each produce different hash output.  Once the passphrase is
   hashed, the output data from the multiple hashes is concatenated,
   first hash leftmost, to produce the key data, with any excess octets
   on the right discarded.

3.7.1.2.  Salted S2K

   This includes a "salt" value in the S2K specifier -- some arbitrary
   data -- that gets hashed along with the passphrase string, to help
   prevent dictionary attacks.

       Octet 0:        0x01
       Octet 1:        hash algorithm
       Octets 2-9:     8-octet salt value

   Salted S2K is exactly like Simple S2K, except that the input to the
   hash function(s) consists of the 8 octets of salt from the S2K
   specifier, followed by the passphrase.

3.7.1.3.  Iterated and Salted S2K

   This includes both a salt and an octet count.  The salt is combined
   with the passphrase and the resulting value is hashed repeatedly.
   This further increases the amount of work an attacker must do to try
   dictionary attacks.

       Octet  0:        0x03
       Octet  1:        hash algorithm
       Octets 2-9:      8-octet salt value
       Octet  10:       count, a one-octet, coded value


   The count is coded into a one-octet number using the following
   formula:

       #define EXPBIAS 6
           count = ((Int32)16 + (c & 15)) << ((c >> 4) + EXPBIAS);

   The above formula is in C, where "Int32" is a type for a 32-bit
   integer, and the variable "c" is the coded count, Octet 10.

   Iterated-Salted S2K hashes the passphrase and salt data multiple
   times.  The total number of octets to be hashed is specified in the
   encoded count in the S2K specifier.  Note that the resulting count
   value is an octet count of how many octets will be hashed, not an
   iteration count.

   Initially, one or more hash contexts are set up as with the other S2K
   algorithms, depending on how many octets of key data are needed.
   Then the salt, followed by the passphrase data, is repeatedly hashed
   until the number of octets specified by the octet count has been
   hashed.  The one exception is that if the octet count is less than
   the size of the salt plus passphrase, the full salt plus passphrase
   will be hashed even though that is greater than the octet count.
   After the hashing is done, the data is unloaded from the hash
   context(s) as with the other S2K algorithms.



   SHA1 github code:
   sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev

   https://www.rfc-editor.org/rfc/rfc4880#section-13.9

   entry point
   // git config --local credential.helper ""


gpg: proc_encryption_packets
gpg: do_proc_packets
gpg: RC:=> 0
gpg: switch2 3
gpg: proc_symkey_enc
gpg: proc_symkey_enc NO DEK
gpg: proc_symkey_enc NO DEK CAST5
gpg: proc_symkey_enc ????????
gpg: proc_symkey_enc ????????
gpg: CAST5 encrypted data
gpg: proc_symkey_enc !!!!!
gpg: passphrase_to_dek_ext
gpg: passphrase_to_dek_ext
gpg: passphrase_to_dek_ext
gpg: passphrase_to_dek_ext
gpg: passphrase_to_dek_ext
gpg: passphrase_to_dek_ext
gpg: keyid !!!! have_static_passphrase password
gpg: hash_passphrase !!!! hash_passphrase password
gpg: hash_passphrase-> algo:2, mode:3, count:255
gpg: KEy? 03
gpg: proc_symkey_enc !!!!!
gpg: proc_symkey_enc *****
gpg: session key: `3:032D070DAD7BF2157918C4649B45AF98'
gpg: RC:=> 0

gpg: switch2 9
gpg: proc_encrypted
gpg: encrypted with 1 passphrase
gpg: DBG: dat: encrypted data
gpg: GOT HERE ec6a8058
gpg: resultresultresult 0

gpg: decrypt_data 
gpg: 0 3 BUFFFFFFFFFFFF 
gpg: 8 blockSize, 37 encrypted data len 
cast_setkey: ec6a806c
cast_setkey: bed67de0
do_cast_setkey x:3201727664,  z:3201727680,  k:3201727696 
do_cast_setkey Km:3201727856, Kr:3201727920
do_cast_setkey key: 3201727968do_decrypt_block
do_cast_setkey x:3201728064,  z:3201728080,  k:3201728096 
do_cast_setkey Km:3966402856, Kr:3966402920
do_cast_setkey key: 3966402668gpg: thekey: 00000000ec6a806c, 16
gpg: ed->len
gpg: gcry_cipher_decrypt
gpg: prefix bed67fe0, 10
gpg: no MDC
gpg: proc_packets 80934070
gpg: do_proc_packets
gpg: RC:=> 0
gpg: switch3 8
gpg: proc_packets 80934070
gpg: do_proc_packets
gpg: RC:=> 0
gpg: switch3 11
gpg: proc_plaintext
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
gpg: resultresultresult 0
gpg: WARNING: message was not integrity protected

Bios disabled SGX
https://en.wikipedia.org/wiki/Software_Guard_Extensions

# https://superuser.com/questions/969238/gnupg-and-cipher-mode-of-operation
CFB custom: https://www.rfc-editor.org/rfc/rfc4880#section-13.9

libmcrypt
./configure --with-included-algos="cast-128 cfb"
make
cd src
./ciphertest
Algorithm: cast-128... ok
Modes:
   cfb: ok

2005-033.pdf
# ad-hoc integrity check feature in OpenPGP which was meant as a “quick check” to determine the correctness of the decrypting symmetric key.
<!-- 
The main difference
with the OpenPGP variant is that a plaintext initialization vector, as described above, is not used,
but instead a random block R is encrypted as the first block of ciphertext. Two bytes of R are
repeated in the second block in order to quickly check whether the session key K is incorrect upon
decryption -->

# Gpg decrypt pseudo
int decrypt_data( void *procctx, PKT_encrypted *ed, DEK *dek )
{
  decode_filter_ctx_t dfx;
  byte *p;
  int rc=0, c, i;
  byte temp[32];
  unsigned blocksize;
  unsigned nprefix;

  blocksize = openpgp_cipher_get_algo_blklen (dek->algo);   

  nprefix = blocksize;

  rc = openpgp_cipher_open (&dfx->cipher_hd, dek->algo,
			    GCRY_CIPHER_MODE_CFB,
			    (GCRY_CIPHER_SECURE
			     | ((ed->mdc_method || dek->algo >= 100)?
				0 : GCRY_CIPHER_ENABLE_SYNC)));
        
  rc = gcry_cipher_setkey (dfx->cipher_hd, dek->key, dek->keylen);
  gcry_cipher_setiv (dfx->cipher_hd, NULL, 0);

  if ( ed->len )
    {
        log_info( "ed->len\n");

      for (i=0; (i < (nprefix+2)) && ed->len; i++, ed->len-- )
        {
          if ( (c=iobuf_get(ed->buf)) == -1 )
            break;
          else
            temp[i] = c;
        }
    }
  // DECRYPTING 'TEMP' bytes
  // 
<!-- 
/* The counterpart to gcry_cipher_encrypt.  */
gcry_error_t gcry_cipher_decrypt (gcry_cipher_hd_t h,
                                  void *out, size_t outsize,
                                  const void *in, size_t inlen); -->
  gcry_cipher_decrypt (dfx->cipher_hd, temp, nprefix+2, NULL, 0);
  <!--// https://www.gnupg.org/documentation/manuals/gcrypt-devel/Working-with-cipher-handles.html
  OpenPGP (as defined in RFC-2440) requires a special sync operation in some places. The following function is used for this:
— Function: gcry_error_t gcry_cipher_sync (gcry_cipher_hd_t h)

    Perform the OpenPGP sync operation on context h. Note that this is a no-op unless the context was created with the flag GCRY_CIPHER_ENABLE_SYNC 
    
    

gcry_error_t
gcry_cipher_ctl( gcry_cipher_hd_t h, int cmd, void *buffer, size_t buflen)
{
  gcry_err_code_t rc = GPG_ERR_NO_ERROR;

  switch (cmd)
    {
    case GCRYCTL_SET_KEY:
      rc = cipher_setkey( h, buffer, buflen );
      break;
    case GCRYCTL_SET_IV:
      cipher_setiv( h, buffer, buflen );
      break;
    case GCRYCTL_RESET:
      cipher_reset (h);
      break;
    case GCRYCTL_CFB_SYNC:
      cipher_sync( h );
      break;


      ...



/****************
 * Used for PGP's somewhat strange CFB mode. Only works if
 * the corresponding flag is set.
 */
static void
cipher_sync( gcry_cipher_hd_t c )
{
    if( (c->flags & GCRY_CIPHER_ENABLE_SYNC) && c->unused ) {
	memmove(c->iv + c->unused, c->iv, c->cipher->blocksize - c->unused );
	memcpy(c->iv, c->lastiv + c->cipher->blocksize - c->unused, c->unused);
	c->unused = 0;
    }
}

      -->
  
  gcry_cipher_sync (dfx->cipher_hd);
  p = temp;
  // Quick bad key detection (passes)
  if (dek->symmetric
      && (p[nprefix-2] != p[nprefix] || p[nprefix-1] != p[nprefix+1]) )
    {
      rc = gpg_error (GPG_ERR_BAD_KEY);
      goto leave;
    }

  dfx->refcount++;
  else{
    log_info("no MDC\n");
    iobuf_push_filter (ed->buf, decode_filter, dfx);
  }

  proc_packets ( procctx, ed->buf );
  ed->buf = NULL;
}


# rfc2440
 OpenPGP CFB mode

   OpenPGP does symmetric encryption using a variant of Cipher Feedback
   Mode (CFB mode). This section describes the procedure it uses in
   detail. This mode is what is used for Symmetrically Encrypted Data
   Packets; the mechanism used for encrypting secret key material is
   similar, but described in those sections above.

   OpenPGP CFB mode uses an initialization vector (IV) of all zeros, and
   prefixes the plaintext with ten octets of random data, such that
   octets 9 and 10 match octets 7 and 8.  It does a CFB "resync" after
   encrypting those ten octets.

   Note that for an algorithm that has a larger block size than 64 bits,
   the equivalent function will be done with that entire block.  For
   example, a 16-octet block algorithm would operate on 16 octets, and
   then produce two octets of check, and then work on 16-octet blocks.




Callas, et. al.             Standards Track                    [Page 58]

RFC 2440                 OpenPGP Message Format            November 1998


   Step by step, here is the procedure:

   1.  The feedback register (FR) is set to the IV, which is all zeros.

   2.  FR is encrypted to produce FRE (FR Encrypted).  This is the
       encryption of an all-zero value.

   3.  FRE is xored with the first 8 octets of random data prefixed to
       the plaintext to produce C1-C8, the first 8 octets of ciphertext.

   4.  FR is loaded with C1-C8.

   5.  FR is encrypted to produce FRE, the encryption of the first 8
       octets of ciphertext.

   6.  The left two octets of FRE get xored with the next two octets of
       data that were prefixed to the plaintext.  This produces C9-C10,
       the next two octets of ciphertext.

   7.  (The resync step) FR is loaded with C3-C10.

   8.  FR is encrypted to produce FRE.

   9.  FRE is xored with the first 8 octets of the given plaintext, now
       that we have finished encrypting the 10 octets of prefixed data.
       This produces C11-C18, the next 8 octets of ciphertext.

   10.  FR is loaded with C11-C18

   11.  FR is encrypted to produce FRE.

   12.  FRE is xored with the next 8 octets of plaintext, to produce the
       next 8 octets of ciphertext.  These are loaded into FR and the
       process is repeated until the plaintext is used up.



5.7. Symmetrically Encrypted Data Packet (Tag 9)

   The Symmetrically Encrypted Data packet contains data encrypted with
   a symmetric-key algorithm. When it has been decrypted, it will
   typically contain other packets (often literal data packets or
   compressed data packets).

   The body of this packet consists of:

     - Encrypted data, the output of the selected symmetric-key cipher
       operating in PGP's variant of Cipher Feedback (CFB) mode.

   The symmetric cipher used may be specified in an Public-Key or
   Symmetric-Key Encrypted Session Key packet that precedes the
   Symmetrically Encrypted Data Packet.  In that case, the cipher
   algorithm octet is prefixed to the session key before it is
   encrypted.  If no packets of these types precede the encrypted data,
   the IDEA algorithm is used with the session key calculated as the MD5
   hash of the passphrase.

   The data is encrypted in CFB mode, with a CFB shift size equal to the
   cipher's block size.  The Initial Vector (IV) is specified as all
   zeros.  Instead of using an IV, OpenPGP prefixes a 10-octet string to
   the data before it is encrypted.  The first eight octets are random,
   and the 9th and 10th octets are copies of the 7th and 8th octets,
   respectively. After encrypting the first 10 octets, the CFB state is
   resynchronized if the cipher block size is 8 octets or less.  The
   last 8 octets of ciphertext are passed through the cipher and the
   block boundary is reset.

   The repetition of 16 bits in the 80 bits of random data prefixed to
   the message allows the receiver to immediately check whether the
   session key is incorrect.



qemu-system-arm -M raspi1ap -kernel build/kernel.armc-uart.rpi1 -serial null -serial mon:stdio -nographic -s -S
gdb-multiarch -tui -ex 'target remote localhost:1234' # note NOT vnc server port but 1234
# Hit enter
layout asm
display/i $pc

break *0x8a14 # Print char to UART line (hello world function anyway)
continue #/c run until breakpoint 
stepi

The most useful thing you can do here is display/i $pc, before using stepi as already suggested in R Samuel Klatchko's answer. This tells gdb to disassemble the current instruction just before printing the prompt each time; then you can just keep hitting Enter to repeat the stepi command.

You can use stepi or nexti (which can be abbreviated to si or ni) to step through your machine code.

# Enter CTRL+c s (to enter single key mode, r:run, c:continue, u:up, d:down, s:step, f:finish)
kill $(ps aux | grep 'qemu' | awk '{print $2}')

set disassemble-next-line on
show disassemble-next-line

https://embeddeddiaries.com/alternative-debugging-prints-for-stm32-without-uart/
https://github.com/cyrus-and/gdb-dashboard
https://www.electronicwings.com/raspberry-pi/raspberry-pi-uart-communication-using-python-and-c


https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging/17341335#17341335

https://stackoverflow.com/questions/22363215/debugging-with-gdb-and-objdump

arm-none-eabi-objdump -D kernel.armc-uart.rpi1 > objdump.list
(https://forums.raspberrypi.com/viewtopic.php?t=203257)

part-5/main$ ../../compiler/gcc-arm-9.2-2019.12-x86_64-arm-none-eabi/arm-none-eabi/bin/objdump -D build/kernel.main.rpi1 > objdump.list
    8820:	eb00927a 	bl	2d210 <printf>
    8824:	eb0006b8 	bl	a30c <printHelloWorld>
    8828:	eb0037cf 	bl	1676c <test>