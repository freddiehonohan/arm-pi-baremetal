/*
    Part of the Raspberry-Pi Bare Metal Tutorials
    https://www.valvers.com/rpi/bare-metal/
    Copyright (c) 2013-2018, Brian Sidebotham

    This software is licensed under the MIT License.
    Please see the LICENSE file included with this software.

*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "rpi-aux.h"
#include "helloworld.h"
#include "data.h"

/** Main function - we'll never return from here */
void kernel_main( unsigned int r0, unsigned int r1, unsigned int atags )
{
    /* Using some print statements with no newline causes the output to be buffered and therefore
       output stagnates, so disable buffering on the stdout FILE */
    setbuf(stdout, NULL);

    /* Initialise the UART */
    RPI_AuxMiniUartInit( 115200, 8 );

    printHelloWorld();
    printf(test_txt);
    while( 1 )
    {
    }
}
