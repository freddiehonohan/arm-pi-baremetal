Various toolchain setup options are documented in the website downloads page.

Lessons:
# ok1:
main.s is the main file and the code is well commented
# ok2:
WAITING
bne = branch if not equal
Be careful to make sure all of your labels are unique. When you write wait1$: you cannot label another line wait1$.

# ok03:
FUNCTIONS

- In higher level code such as C or C++, functions are part of the language itself. In assembly code, functions are just ideas we have.
- Ideally we want to be able to set our registers to some input values, branch to an address, and expect that at some point the code will branch back to our code having set the registers to output values. This is what a function is in assembly code.
- a standard called the Application Binary Interface (ABI) was devised for each assembly language which is an agreement on how functions should be run
- When a function completes it has to branch back to the code that started it. This means it must know the address of the code that started it. To facilitate this, there is a special register called lr (link register) which always holds the address of the instruction after the one that called this function.
Table 1.1 ARM ABI register usage
Register	Brief	Preserved	Rules
r0	Argument and result	No	r0 and r1 are used for passing the first two arguments to functions, and returning the results of functions. If a function does not use them for a return value, they can take any value after a function.
r1	Argument and result	No
r2	Argument	No	r2 and r3 are used for passing the second two arguments to functions. There values after a function is called can be anything.
r3	Argument	No
r4	General purpose	Yes	r4 to r12 are used for working values, and their value after a function is called must be the same as before.
r5	General purpose	Yes
r6	General purpose	Yes
r7	General purpose	Yes
r8	General purpose	Yes
r9	General purpose	Yes
r10	General purpose	Yes
r11	General purpose	Yes
r12	General purpose	Yes
lr	Return address	No	lr is the address to branch back to when a function is finished, but this does have to contain the same address after the function has finished.
sp	Stack pointer	Yes	sp is the stack pointer, described below. Its value must be the same after the function has finished.
Often functions need to use more registers than just r0 to r3. But, since r4 to r12 must stay the same after the method has run, they must be saved somewhere. We save them on something called the stack.

- The .globl GetGpioAddress command is a message to the assembler to make the label GetGpioAddress accessible to all files. This means that in our main.s file we can branch to the label GetGpioAddress even though it is not defined in that file (it's in gpio.s).

- lr = link register
- pc = prev code ?
- mov pc,lr copies the value in lr to pc. As mentioned earlier lr always contains the address of the code that we have to go back to when a method finishes. pc is a special register which always contains the address of the next instruction to be run. A normal branch command just changes the value of this register. By copying the value in lr to pc we just change the next line to be run to be the one we were told to go back to.

- A reasonable question would now be, how would we actually run this code? A special type of branch bl does what we need. It branches to a label like a normal branch, but before it does it updates lr to contain the address of the line after the branch. That means that when the function finishes, the line it will go back to will be the one after the bl command. 

# ok04
- Just like the GPIO Controller, the timer has an address. In this case, the timer is based at 2000300016. Reading the manual, we find the following table:

Table 1.1 GPIO Controller Registers
Address	Size / Bytes	Name	Description	Read or Write
20003000	4	Control / Status	Register used to control and clear timer channel comparator matches.	RW
20003004	8	Counter	A counter that increments at 1MHz.	R
2000300C	4	Compare 0	0th Comparison register.	RW
20003010	4	Compare 1	1st Comparison register.	RW
20003014	4	Compare 2	2nd Comparison register.	RW
20003018	4	Compare 3	3rd Comparison register.	RW

- The timer is the only way the Pi can keep time. Most computers have a battery powered clock to keep time when off.
- The complicated part about this method, is that the counter is an 8 byte value, but each register only holds 4 bytes. 

# ok05
## Data
- To differentiate between data and code, we put all the data in the .data section.
- I've used two new commands here. .align and .int. .align ensures alignment of the following data to a specified power of 2. In this case I've used .align 2 which means that this data will definitely be placed at an address which is a multiple of 22 = 4. It is really important to do this, because the ldr instruction we used to read memory only works at addresses that are multiples of 4.
- The .int command copies the constant after it into the output directly. That means that 111111111010101000100010001010102 will be placed into the output, and so the label pattern actually labels this piece of data as pattern.
- hex data how (C code)?

# screen01
- Table 3.1 Mailbox Addresses
Address	Size / Bytes	Name	Description	Read / Write
2000B880	4	Read	Receiving mail.	R
2000B890	4	Poll	Receive without retrieving.	R
2000B894	4	Sender	Sender information.	R
2000B898	4	Status	Information.	R
2000B89C	4	Configuration	Settings.	RW
2000B8A0	4	Write	Sending mail.	W
In order to send a message to a particular mailbox:

The sender waits until the Status field has a 0 in the top bit.
The sender writes to Write such that the lowest 4 bits are the mailbox to write to, and the upper 28 bits are the message to write.
In order to read a message:

The receiver waits until the Status field has a 0 in the 30th bit.
The receiver reads from Read.
The receiver confirms the message is for the correct mailbox, and tries again if not.
- Framebuffer defined in framebuffer.s
# Running on QEMU
$ qemu-system-aarch64 -M raspi3 -kernel kernel8.img -serial stdio ?! ok not working



So let's try uart output then with rpi3

