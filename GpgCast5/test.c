/*
SHA1 tests by Philip Woolford <woolford.philip@gmail.com>
100% Public Domain
*/

#include "sha1.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "cipher.h"
#include "packet.h"
#include "mainproc.h"
// xxd --include testAAA.txt.gpg 
unsigned char testAAA_txt_gpg[] = {
  0x8c, 0x0d, 0x04, 0x03, 0x03, 0x02, 0x60, 0xa2, 0x3c, 0xee, 0x14, 0xd2,
  0xdb, 0x0c, 0xff, 0xc9, 0x25, 0xd8, 0x98, 0x2e, 0x2b, 0x2f, 0x13, 0xc8,
  0x94, 0xd0, 0x22, 0x80, 0x1f, 0x33, 0x1a, 0x6c, 0xd2, 0x49, 0x35, 0x61,
  0xe4, 0xc5, 0x98, 0x74, 0x04, 0x76, 0x56, 0xa9, 0xdb, 0x3c, 0x91, 0x9c,
  0x23, 0xb4, 0x6c, 0xc9, 0xd6, 0x04
};
unsigned int testAAA_txt_gpg_len = 54;
// 0x8c 0xd 0x4 0x3 0x3 0x2 0x60 0xa2 0x3c 0xee 0x14 0xd2 0xdb 0xc 0xff 0xc9 0x25 0xd8 0x98 0x2e 0x2b 0x2f 0x13 0xc8 0x94 0xd0 0x22 0x80 0x1f 0x33 0x1a 0x6c 0xd2 0x49 0x35 0x61 0xe4 0xc5 0x98 0x74 0x4 0x76 0x56 0xa9 0xdb 0x3c 0x91 0x9c 0x23 0xb4 0x6c 0xc9 0xd6 0x4
/* xxd testAAA.txt.gpg 
00000000: 8c0d 0403 0302 60a2 3cee 14d2 db0c ffc9  ......`.<.......
00000010: 25d8 982e 2b2f 13c8 94d0 2280 1f33 1a6c  %...+/...."..3.l
00000020: d249 3561 e4c5 9874 0476 56a9 db3c 919c  .I5a...t.vV..<..
00000030: 23b4 6cc9 d604                           #.l...*/
//len: 54, size:8192, start:17

/*./gpg2 --list-packets testAAA.txt.gpg 
 gpg --list-packets testAAA.txt.gpg 
gpg: CAST5 encrypted data
gpg: encrypted with 1 passphrase
gpg: WARNING: message was not integrity protected
gpg: Use the option '--ignore-mdc-error' to decrypt anyway.
gpg: decryption forced to fail!
# off=0 ctb=8c tag=3 hlen=2 plen=13
:symkey enc packet: version 4, cipher 3, s2k 3, hash 2
        salt 60A23CEE14D2DB0C, count 65011712 (255)
# off=15 ctb=c9 tag=9 hlen=2 plen=37 new-ctb
:encrypted data packet:
        length: 37
# off=27 ctb=a3 tag=8 hlen=1 plen=0 indeterminate
:compressed packet: algo=1
# off=29 ctb=ac tag=11 hlen=2 plen=146
:literal data packet:
        mode b (62), created 1658669658, name="testAAA.txt",
        raw data: 129 bytes
*/
/*
pgpdump testAAA.txt.gpg 
Old: Symmetric-Key Encrypted Session Key Packet(tag 3)(13 bytes)
        New version(4)
        Sym alg - CAST5(sym 3)
        Iterated and salted string-to-key(s2k 3):
                Hash alg - SHA1(hash 2)
                Salt - 60 a2 3c ee 14 d2 db 0c 
                Count - 65011712(coded count 255)
New: Symmetrically Encrypted Data Packet(tag 9)(37 bytes)
        Encrypted data [sym alg is specified in sym-key encrypted session key]*/
// **** gpg: session key: `3:A34A427AB8A4874E94EF912211E86239'
// **** '3:' pertains to cipher ****

/*
pgpdump g10/testAAA.txt.gpg pgpdump g10/testAAA.txt.gpg 
Old: Symmetric-Key Encrypted Session Key Packet(tag 3)(13 bytes)
        New version(4)
        Sym alg - CAST5(sym 3)
        Iterated and salted string-to-key(s2k 3):
                Hash alg - SHA1(hash 2)
                Salt - ce c2 52 33 34 d2 ae 99 
                Count - 65011712(coded count 255)
New: Symmetrically Encrypted Data Packet(tag 9)(37 bytes)
        Encrypted data [sym alg is specified in sym-key encrypted session key]
*/
/* Byte1: gpg packet tag
Byte 2: packet length
3: version tag
4: algo
5: s2k
6: hash algo
7 - 15: 8 byte salt
16: count (hash algo iterations)
17: gpg packet tag
18: packet length
*/
/* Test Vector 1 */

/*

3.7.1.3.  Iterated and Salted S2K

   This includes both a salt and an octet count.  The salt is combined
   with the passphrase and the resulting value is hashed repeatedly.
   This further increases the amount of work an attacker must do to try
   dictionary attacks.

       Octet  0:        0x03
       Octet  1:        hash algorithm
       Octets 2-9:      8-octet salt value
       Octet  10:       count, a one-octet, coded value



   The count is coded into a one-octet number using the following
   formula:

       #define EXPBIAS 6
           count = ((Int32)16 + (c & 15)) << ((c >> 4) + EXPBIAS);

   The above formula is in C, where "Int32" is a type for a 32-bit
   integer, and the variable "c" is the coded count, Octet 10.

   Iterated-Salted S2K hashes the passphrase and salt data multiple
   times.  The total number of octets to be hashed is specified in the
   encoded count in the S2K specifier.  
   ********************************************************************
   Note that the resulting count
   value is an octet count of how many octets will be hashed, not an
   iteration count.
   **********************************************************************
   Initially, one or more hash contexts are set up as with the other S2K
   algorithms, depending on how many octets of key data are needed.
   Then the salt, followed by the passphrase data, is repeatedly hashed
   until the number of octets specified by the octet count has been
   hashed.  The one exception is that if the octet count is less than
   the size of the salt plus passphrase, the full salt plus passphrase
   will be hashed even though that is greater than the octet count.
   After the hashing is done, the data is unloaded from the hash
   context(s) as with the other S2K algorithms.*/

   // https://raysnotebook.info/computing/crypto-gnupg-s2k.html
   // The passphrase is "abc", and the count is 1,408. Thus the salted passphrase appears 128 times: (8+3)×128 = 1,408. 

#define EXPBIAS 6

// Converts a string of hexadecimal characters into a byte array
// https://stackoverflow.com/a/35452093
uint8_t* datahex(char* string) {

    if(string == NULL) 
       return NULL;

    size_t slength = strlen(string);
    if((slength % 2) != 0) // must be even
       return NULL;

    size_t dlength = slength / 2;

    uint8_t* data = malloc(dlength);
    memset(data, 0, dlength);

    size_t index = 0;
    while (index < slength) {
        char c = string[index];
        int value = 0;
        if(c >= '0' && c <= '9')
          value = (c - '0');
        else if (c >= 'A' && c <= 'F') 
          value = (10 + (c - 'A'));
        else if (c >= 'a' && c <= 'f')
          value = (10 + (c - 'a'));
        else {
          free(data);
          return NULL;
        }

        data[(index/2)] += value << (((index + 1) % 2) * 4);

        index++;
    }

    return data;
}

void testS2k(
    void
)
{
  // char const string[] = "abc";
  char const passphrase[] = "password";
  // Skip 6 bytes and read the 8 byte salt from above 
  int byteOffset = 6;

  unsigned char salt [8]; //{0xce, 0xc2, 0x52, 0x33, 0x34, 0xd2, 0xae, 0x99};
  for(int i=0;i<8;i++){
    salt[i]=testAAA_txt_gpg[byteOffset+i];
  }
  // char const expect[] = "a9993e364706816aba3e25717850c26c9cd0d89d";
  char const expected[] = "032D070DAD7BF2157918C4649B45AF98";

  byteOffset+=8;

  int passphraseLength = sizeof(passphrase);
  int saltLength = sizeof(salt);
  printf("passphrase: %i\n",passphraseLength);
  printf("salt: %i\n",saltLength);
  printf("passphrase+salt->length: %i\n",passphraseLength+saltLength);

unsigned char saltPlusPassphrase[sizeof(salt)+strlen(passphrase)];
memcpy(saltPlusPassphrase,salt,sizeof(salt));
memcpy(saltPlusPassphrase+sizeof(salt),passphrase,sizeof(passphrase));

  printf("%.*s\n", (int) sizeof (saltPlusPassphrase), saltPlusPassphrase);
  printf("passphrase+salt: %s\n",saltPlusPassphrase);



  int c = 0xff; // Swap this out for actual byte count
  int octetExpansionCount = ((int32_t)16 + (c & 15)) << ((c >> 4) + EXPBIAS);

  printf("octetExpansionCount: %i\n",octetExpansionCount);


  int totalLen = sizeof(saltPlusPassphrase);
  printf("saltPlusPassphraseLength: %i\n",totalLen);

  /*int repeatCount = octetExpansionCount/totalLen;

  printf("repeatCount: %i\n",repeatCount);*/

  // Now we multiply the total length by the repeat count
  // And loop copying a single byte at a time, wrapping around the end to the start

  int byteIndex = 0;
  // unsigned char saltPlusPassphraseExpanded[octetExpansionCount];
  unsigned char* b = (unsigned char*)malloc(octetExpansionCount);
  for(int i = 0;i<octetExpansionCount;i++){

    memcpy(b+(i*sizeof(unsigned char)),&saltPlusPassphrase[byteIndex],sizeof(unsigned char));
    // memcpy(saltPlusPassphraseExpanded+(i*sizeof(unsigned char)),&saltPlusPassphrase[byteIndex],sizeof(unsigned char));

    byteIndex++;
    if(byteIndex==totalLen)// Wrap around end
      byteIndex = 0;
  }

  printf("passphrase+salt EXPANDED: %li\n",strlen((char*)b));

  char result[21];
  /* calculate hash */
  SHA1( result,(char*)b, strlen((char*)b) );

  char hexresult[41];
  size_t offset;
  
  /* format the hash for comparison */
  for( offset = 0; offset < 20; offset++) {
    sprintf( ( hexresult + (2*offset)), "%02x", result[offset]&0xff);
  }

  printf("Hex result: %s\n",hexresult);
  printf("Expected: %s\n", expected);

  // int decrypt_data( void *ctx, PKT_encrypted *ed, DEK *dek );
  // CTX c, PACKET *pkt )
/*
struct mainproc_context
{
  struct mainproc_context *anchor; // May be useful in the future. 
  DEK *dek;
  KBNODE list; // The current list of packets. 
  int have_data;
  IOBUF iobuf;     // Used to get the filename etc. 
  int trustletter; // Temporary usage in list_node. 
  ulong symkeys;
  struct kidlist_item *pkenc_list; // List of encryption packets. 
  int any_sig_seen;                // Set to true if a signature packet has been seen. 
};
*/
// CTX context;
/*
DEK:
  int algo;
  int keylen;
  int algo_info_printed;
  int use_mdc;
  int symmetric;
  byte key[32]; // This is the largest used keylen (256 bit).
  char s2k_cacheid[1+16+1];
*/

// DEK *dek;// = //passphrase_to_dek ( NULL, 0, algo, s2k, 3, NULL, NULL );

// typedef struct packet_struct PACKET;
// 	PKT_encrypted	*encrypted;	/* PKT_ENCRYPTED[_MDC] */

// typedef struct {
//     u32  len;		  /* length of encrypted data */
//     int  extralen;        /* this is (blocksize+2) */
//     byte new_ctb;	  /* uses a new CTB */
//     byte is_partial;      /* partial length encoded */
//     byte mdc_method;	  /* > 0: integrity protected encrypted data packet */
//     iobuf_t buf;	  /* IOBUF reference */
// } PKT_encrypted;

// printf("%d, %d, %d, %d, %d, %d\n", ed->buf, ed->extralen, ed->is_partial, ed->len, ed->mdc_method, ed->new_ctb);
// buf: 1319354480, 0, 0, 37, 0, 1
// printf("%d, %d, %d, %d, %d, %d\n", dek->algo, dek->key, dek->keylen, dek->s2k_cacheid, dek->symmetric, dek->use_mdc);


// PKT_encrypted	*encrypted;
// iobuf_t buf;

// typedef struct iobuf_struct *iobuf_t;
// typedef struct iobuf_struct *IOBUF;  /* Compatibility with gpg 1.4. */

// /* fixme: we should hide most of this stuff */
// struct iobuf_struct
// {
//   int use;			/* 1 input , 2 output, 3 temp */
//   off_t nlimit;
//   off_t nbytes;			/* Used together with nlimit. */
//   off_t ntotal;			/* Total bytes read (position of stream). */
//   int nofast;			/* Used by the iobuf_get (). */
//   void *directfp;
//   struct
//   {
//     size_t size;		/* Allocated size */
//     size_t start;		/* Number of invalid bytes at the
//                                    begin of the buffer */
//     size_t len;			/* Currently filled to this size */
//     byte *buf;
//   } d;

//   int filter_eof;
//   int error;
//   int (*filter) (void *opaque, int control,
// 		 iobuf_t chain, byte * buf, size_t * len);
//   void *filter_ov;		/* Value for opaque */
//   int filter_ov_owner;
//   char *real_fname;
//   iobuf_t chain;		/* Next iobuf used for i/o if any
//                                    (passed to filter) */
//   int no, subno;
//   const char *desc;
//   void *opaque;			/* Can be used to hold any information
//                                    this value is copied to all
//                                    instances */
// };
/*
   // ((ed->buf)->nofast || (ed->buf)->d.start >= (ed->buf)->d.len )? iobuf_readbyte((ed->buf)) : ( (ed->buf)->nbytes++, (ed->buf)->d.buf[(ed->buf)->d.start++] ) )
          // iobuf_push_filter
  if (a->directfp)
  if (a->use == 2 && (rc = iobuf_flush (a)))
  
  if (a->directfp)
      size_t newsize = a->d.size + IOBUF_BUFFER_SIZE;

  else if (!a->filter)
    a->error = rc;*/
  /*
   // iobuf_get
          // ( ((ed->buf)->nofast || (ed->buf)->d.start >= (ed->buf)->d.len )? iobuf_readbyte((ed->buf)) : ( (ed->buf)->nbytes++, (ed->buf)->d.buf[(ed->buf)->d.start++] ) )
          // iobuf_push_filter
  if (a->directfp)

  if (a->use == 2 && (rc = iobuf_flush (a)))
  if (a->directfp)
      size_t newsize = a->d.size + IOBUF_BUFFER_SIZE;

  else if (!a->filter)
  rc = a->filter (a->filter_ov, IOBUFCTRL_FLUSH, a->chain, a->d.buf, &len);
  else if (rc)
    a->error = rc;
          if ( (c=iobuf_get(ed->buf)) == -1 )
  f (ov, IOBUFCTRL_DESC, NULL, (byte *) & a->desc, &dummy_len);*/
// encrypted->buf = 

	//	decrypt_data( context, encrypted, dek );
    return;
}

int main(
    void
)
{
    testS2k();
    return 0;
}
