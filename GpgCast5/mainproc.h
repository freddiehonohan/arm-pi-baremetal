
struct kidlist_item
{
  struct kidlist_item *next;
  u32 kid[2];
  int pubkey_algo;
  int reason;
};

/* Object used to describe a keyblok node.  */
typedef struct kbnode_struct *KBNODE;

struct kbnode_struct
{
  KBNODE next;
  PACKET *pkt;
  int flag;
  int private_flag;
  ulong recno; /* used while updating the trustdb */
};

#define is_deleted_kbnode(a) ((a)->private_flag & 1)
#define is_cloned_kbnode(a) ((a)->private_flag & 2)

typedef struct mainproc_context *CTX;
struct mainproc_context
{
  struct mainproc_context *anchor; /* May be useful in the future. */
  DEK *dek;
  KBNODE list; /* The current list of packets. */
  int have_data;
  IOBUF iobuf;     /* Used to get the filename etc. */
  int trustletter; /* Temporary usage in list_node. */
  ulong symkeys;
  struct kidlist_item *pkenc_list; /* List of encryption packets. */
  int any_sig_seen;                /* Set to true if a signature packet has been seen. */
};