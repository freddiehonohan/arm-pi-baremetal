/* encr-data.c -  process an encrypted data packet
 * Copyright (C) 1998, 1999, 2000, 2001, 2005,
 *               2006, 2009, 2012 Free Software Foundation, Inc.
 *
 * This file is part of GnuPG.
 *
 * GnuPG is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GnuPG is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// #define openpgp_cipher_open(_a,_b,_c,_d) gcry_cipher_open((_a),map_cipher_openpgp_to_gcry((_b)),(_c),(_d))
#define openpgp_cipher_open(_a, _b, _c, _d) gcry_cipher_open((_a), (_b), (_c), (_d))

// #include "gpg.h"
#include "visibility.h"
#include "util.h"
#include "packet.h"
#include "cipher.h"
// #include "options.h"
// #include "i18n.h"
// #include "status.h"
#include "logging.h"
#include "mainproc.h"
#include "gcrypt.h"

int parse_packet(iobuf_t inp, PACKET *ret_pkt);


static void proc_encrypted(CTX c, PACKET *pkt)
{
  log_info("proc_encrypted\n");

  int result = 0;

  c->dek = NULL; // passphrase_to_dek ( NULL, 0, algo, s2k, 3, NULL, NULL );

  if (!result)
  {
    log_info(("resultresultresult %i\n"), result);

    result = decrypt_data(c, pkt->pkt.encrypted, c->dek);
    log_info(("resultresultresult2 %i\n"), result);
  }
  /*
      if( result == -1 )
    ;
      else if( !result || (gpg_err_code (result) == GPG_ERR_BAD_SIGNATURE
                           && opt.ignore_mdc_error)) {
    write_status( STATUS_DECRYPTION_OKAY );
    if( opt.verbose > 1 )
        log_info(_("decryption okay\n"));
    if( pkt->pkt.encrypted->mdc_method && !result )
        write_status( STATUS_GOODMDC );
    else if(!opt.no_mdc_warn)
        log_info (_("WARNING: message was not integrity protected\n"));
    if(opt.show_session_key)
      {
        int i;
        char *buf = xmalloc ( c->dek->keylen*2 + 20 );
        sprintf ( buf, "%d:", c->dek->algo );
        for(i=0; i < c->dek->keylen; i++ ){
          sprintf(buf+strlen(buf), "%02X", c->dek->key[i] );
        log_info( "session key %i: `%02X'\n", i, c->dek->key[i]);
      }
          // sprintf(buf+strlen(buf), "%02X", c->dek->key[i] );
        // log_info( "session key BUF: `%X'\n", c->dek->key );
        log_info( "session key: `%s'\n", buf );
        write_status_text ( STATUS_SESSION_KEY, buf );
      }
      }
      else if( result == G10ERR_BAD_SIGN ) {
    log_error(_("WARNING: encrypted message has been manipulated!\n"));
    write_status( STATUS_BADMDC );
    write_status( STATUS_DECRYPTION_FAILED );
      }
      else {
          if (gpg_err_code (result) == GPG_ERR_BAD_KEY
        && *c->dek->s2k_cacheid != '\0')
      {
        log_debug(_("cleared passphrase cached with ID: %s\n"),
            c->dek->s2k_cacheid);
        passphrase_clear_cache (NULL, c->dek->s2k_cacheid, 0);
      }
    write_status( STATUS_DECRYPTION_FAILED );
    log_error(_("decryption failed: %s\n"), g10_errstr(result));
     Hmmm: does this work when we have encrypted using multiple
     * ways to specify the session key (symmmetric and PK)
      }
      xfree(c->dek); c->dek = NULL;
      free_packet(pkt);
      c->last_was_session_key = 0;
      write_status( STATUS_END_DECRYPTION );*/
}

static int do_proc_packets(CTX c, IOBUF a);

int do_proc_packets(CTX c, IOBUF a)
{
  log_info("do_proc_packets\n");
  PACKET *pkt = xmalloc(sizeof *pkt);
  int rc = 0;
  /* int any_data=0;
   int newpkt; */

  c->iobuf = a;
  init_packet(pkt);
  while ((rc = parse_packet(a, pkt)) != -1)
  {
    // any_data = 1;

    log_info("RC:=> %i\n", rc);
      log_info("switch3 %i\n", pkt->pkttype);

      switch (pkt->pkttype)
      {
      case PKT_ENCRYPTED:
      case PKT_ENCRYPTED_MDC:
        proc_encrypted(c, pkt);
        break; 
      default:
        break;
      }
    }

    return rc;
  }

  typedef struct decode_filter_context_s
  {
    gcry_cipher_hd_t cipher_hd;
    gcry_md_hd_t mdc_hash;
    char defer[22];
    int defer_filled;
    int eof_seen;
    int refcount;
  } * decode_filter_ctx_t;

  /* Helper to release the decode context.  */
  void
  release_dfx_context(decode_filter_ctx_t dfx)
  {
    if (!dfx)
      return;

    assert(dfx->refcount);
    if (!--dfx->refcount)
    {
      gcry_cipher_close(dfx->cipher_hd);
      dfx->cipher_hd = NULL;
      gcry_md_close(dfx->mdc_hash);
      dfx->mdc_hash = NULL;
      // xfree (dfx);
    }
  }

  int proc_packets(void *anchor, IOBUF a)
  {
    log_info("proc_packets\n");
    int rc;
    CTX c = xmalloc_clear(sizeof *c);

    c->anchor = anchor;
    rc = do_proc_packets(c, a);
    xfree(c);
    return rc;
  }

  int decode_filter(void *opaque, int control, IOBUF a, byte *buf, size_t *ret_len)
  {
    decode_filter_ctx_t fc = opaque;
    size_t n, size = *ret_len;
    int rc = 0;

    if (control == IOBUFCTRL_UNDERFLOW)
    {
      assert(a);
      n = iobuf_read(a, buf, size);
      if (n == -1)
        n = 0;
      if (n)
      {
        if (fc->cipher_hd)
          gcry_cipher_decrypt(fc->cipher_hd, buf, n, NULL, 0);
      }
      else
        rc = -1; /* EOF */
      *ret_len = n;
    }
    else if (control == IOBUFCTRL_FREE)
    {
      release_dfx_context(fc);
    }
    else if (control == IOBUFCTRL_DESC)
    {
      *(char **)buf = "decode_filter";
    }
    return rc;
  }

  /****************
   * Decrypt the data, specified by ED with the key DEK.
   */
  int decrypt_data(void *procctx, PKT_encrypted *ed, DEK *dek)
  {

    log_info("decrypt_data \n");
    decode_filter_ctx_t dfx;
    byte *p;
    int rc = 0, c, i;
    byte temp[32];
    unsigned blocksize;
    unsigned nprefix;

    dfx = xtrycalloc(1, sizeof *dfx);
    if (!dfx)
      return -1;// gpg_error_from_syserror();
    dfx->refcount = 1;
    /*
      if ( opt.verbose && !dek->algo_info_printed )
        {
          if (!openpgp_cipher_test_algo (dek->algo))
            log_info (("%s encrypted data\n"),
                      openpgp_cipher_algo_name (dek->algo));
          else
            log_info (("encrypted with unknown algorithm %d\n"), dek->algo );
          dek->algo_info_printed = 1;
        }*/

    {
      char buf[20];

      snprintf(buf, sizeof buf, "%d %d", ed->mdc_method, dek->algo);
      // write_status_text (STATUS_DECRYPTION_INFO, buf);
      log_info(("%s BUFFFFFFFFFFFF \n"), buf);
    }
    /*
      rc = openpgp_cipher_test_algo (dek->algo);
      if (rc){
        log_info("goto leave\n");
        goto leave;
      }*/
    // #define CAST5_BLOCKSIZE 8

    blocksize = 8; // openpgp_cipher_get_algo_blklen (dek->algo);
    /*if ( !blocksize || blocksize > 16 )
      log_fatal ("unsupported blocksize %u\n", blocksize );*/
    nprefix = blocksize;
    if (ed->len && ed->len < (nprefix + 2))
        printf("BUG");
    log_info(("%i blockSize, %i encrypted data len \n"), blocksize, ed->len);
    /*
      if ( ed->mdc_method )
        {
        log_info("mdc_method\n");
          if (gcry_md_open (&dfx->mdc_hash, ed->mdc_method, 0 ))
            BUG ();
          if ( DBG_HASHING )
            gcry_md_debug (dfx->mdc_hash, "checkmdc");
        }
    */

    rc = openpgp_cipher_open(&dfx->cipher_hd, dek->algo,
                             GCRY_CIPHER_MODE_CFB,
                             (GCRY_CIPHER_SECURE | ((ed->mdc_method || dek->algo >= 100) ? 0 : GCRY_CIPHER_ENABLE_SYNC)));
    if (rc)
    {
      /* We should never get an error here cause we already checked
       * that the algorithm is available.  */
        printf("BUG");
    }

    // log_info( "thekey: %016x, %i", dek->key, dek->keylen );
    rc = gcry_cipher_setkey(dfx->cipher_hd, dek->key, dek->keylen);
    if (gpg_err_code(rc) == GPG_ERR_WEAK_KEY)
    {
      log_info(("WARNING: message was encrypted with"
                " a weak key in the symmetric cipher.\n"));
      rc = 0;
    }
    else if (rc)
    {
      // log_error("key setup failed: %s\n", g10_errstr(rc) );
      goto leave;
    }

    if (!ed->buf)
    {
      log_error(("problem handling encrypted packet\n"));
      goto leave;
    }

    gcry_cipher_setiv(dfx->cipher_hd, NULL, 0);

    if (ed->len)
    {
      log_info("ed->len\n");

      for (i = 0; i < (nprefix + 2) && ed->len; i++, ed->len--)
      {
        if ((c = iobuf_get(ed->buf)) == -1)
          break;
        else
          temp[i] = c;
      }
    }
    else
    {
      log_info("ed->len NOT\n");

      for (i = 0; i < (nprefix + 2); i++)
        if ((c = iobuf_get(ed->buf)) == -1)
          break;
        else
          temp[i] = c;
    }

    gcry_cipher_decrypt(dfx->cipher_hd, temp, nprefix + 2, NULL, 0);
    log_info("gcry_cipher_decrypt\n");
    gcry_cipher_sync(dfx->cipher_hd);
    p = temp;
    // log_info( "prefix %x, %i", temp, nprefix+2 );
    // Quick bad key detection (passes)
    if (dek->symmetric && (p[nprefix - 2] != p[nprefix] || p[nprefix - 1] != p[nprefix + 1]))
    {
      rc = gpg_error(GPG_ERR_BAD_KEY);
      goto leave;
    }

    if (dfx->mdc_hash)
    {
      gcry_md_write(dfx->mdc_hash, temp, nprefix + 2);
      log_info("gcry_md_write\n");
    }

    dfx->refcount++;
    if (ed->mdc_method)
    {
      /*log_info("MDC\n");
        iobuf_push_filter ( ed->buf, mdc_decode_filter, dfx );*/
    }
    else
    {
      log_info("no MDC\n");
      iobuf_push_filter(ed->buf, decode_filter, dfx);
    }

    proc_packets(procctx, ed->buf);
    ed->buf = NULL;
    if (ed->mdc_method && dfx->eof_seen == 2)
      rc = gpg_error(GPG_ERR_INV_PACKET);
    else if (ed->mdc_method)
    {
      /* We used to let parse-packet.c handle the MDC packet but this
         turned out to be a problem with compressed packets: With old
         style packets there is no length information available and
         the decompressor uses an implicit end.  However we can't know
         this implicit end beforehand (:-) and thus may feed the
         decompressor with more bytes than actually needed.  It would
         be possible to unread the extra bytes but due to our weird
         iobuf system any unread is non reliable due to filters
         already popped off.  The easy and sane solution is to care
         about the MDC packet only here and never pass it to the
         packet parser.  Fortunatley the OpenPGP spec requires a
         strict format for the MDC packet so that we know that 22
         bytes are appended.  */
      int datalen = gcry_md_get_algo_dlen(ed->mdc_method);

      assert(dfx->cipher_hd);
      assert(dfx->mdc_hash);
      gcry_cipher_decrypt(dfx->cipher_hd, dfx->defer, 22, NULL, 0);
      gcry_md_write(dfx->mdc_hash, dfx->defer, 2);
      gcry_md_final(dfx->mdc_hash);

      if (dfx->defer[0] != '\xd3' || dfx->defer[1] != '\x14')
      {
        log_error("mdc_packet with invalid encoding\n");
        rc = gpg_error(GPG_ERR_INV_PACKET);
      }
      else if (datalen != 20 || memcmp(gcry_md_read(dfx->mdc_hash, 0),
                                       dfx->defer + 2, datalen))
        rc = gpg_error(GPG_ERR_BAD_SIGNATURE);
      /*log_printhex("MDC message:", dfx->defer, 22);//
      log_printhex("MDC calc:", gcry_md_read (dfx->mdc_hash,0), datalen);// */
    }

  leave:
    release_dfx_context(dfx);
    return rc;
  }
