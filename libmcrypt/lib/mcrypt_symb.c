#include "mcrypt_internal.h"

/* This is automatically created. Don't touch... */

extern cast_128_LTX__mcrypt_set_key();
extern cast_128_LTX__mcrypt_encrypt();
extern cast_128_LTX__mcrypt_decrypt();
extern cast_128_LTX__mcrypt_get_size();
extern cast_128_LTX__mcrypt_get_block_size();
extern cast_128_LTX__is_block_algorithm();
extern cast_128_LTX__mcrypt_get_key_size();
extern cast_128_LTX__mcrypt_get_supported_key_sizes();
extern cast_128_LTX__mcrypt_get_algorithms_name();
extern cast_128_LTX__mcrypt_self_test();
extern cast_128_LTX__mcrypt_algorithm_version();
extern cfb_LTX__init_mcrypt();
extern cfb_LTX__mcrypt_set_state();
extern cfb_LTX__mcrypt_get_state();
extern cfb_LTX__end_mcrypt();
extern cfb_LTX__mcrypt();
extern cfb_LTX__mdecrypt();
extern cfb_LTX__has_iv();
extern cfb_LTX__is_block_mode();
extern cfb_LTX__is_block_algorithm_mode();
extern cfb_LTX__mcrypt_get_modes_name();
extern cfb_LTX__mcrypt_mode_get_size();
extern cfb_LTX__mcrypt_mode_version();

const mcrypt_preloaded mps[] = {
	{"cast-128", NULL}, 
	{"cast_128_LTX__mcrypt_set_key", cast_128_LTX__mcrypt_set_key},
	{"cast_128_LTX__mcrypt_encrypt", cast_128_LTX__mcrypt_encrypt},
	{"cast_128_LTX__mcrypt_decrypt", cast_128_LTX__mcrypt_decrypt},
	{"cast_128_LTX__mcrypt_get_size", cast_128_LTX__mcrypt_get_size},
	{"cast_128_LTX__mcrypt_get_block_size", cast_128_LTX__mcrypt_get_block_size},
	{"cast_128_LTX__is_block_algorithm", cast_128_LTX__is_block_algorithm},
	{"cast_128_LTX__mcrypt_get_key_size", cast_128_LTX__mcrypt_get_key_size},
	{"cast_128_LTX__mcrypt_get_supported_key_sizes", cast_128_LTX__mcrypt_get_supported_key_sizes},
	{"cast_128_LTX__mcrypt_get_algorithms_name", cast_128_LTX__mcrypt_get_algorithms_name},
	{"cast_128_LTX__mcrypt_self_test", cast_128_LTX__mcrypt_self_test},
	{"cast_128_LTX__mcrypt_algorithm_version", cast_128_LTX__mcrypt_algorithm_version},
	{"cfb", NULL}, 
	{"cfb_LTX__init_mcrypt", cfb_LTX__init_mcrypt},
	{"cfb_LTX__mcrypt_set_state", cfb_LTX__mcrypt_set_state},
	{"cfb_LTX__mcrypt_get_state", cfb_LTX__mcrypt_get_state},
	{"cfb_LTX__end_mcrypt", cfb_LTX__end_mcrypt},
	{"cfb_LTX__mcrypt", cfb_LTX__mcrypt},
	{"cfb_LTX__mdecrypt", cfb_LTX__mdecrypt},
	{"cfb_LTX__has_iv", cfb_LTX__has_iv},
	{"cfb_LTX__is_block_mode", cfb_LTX__is_block_mode},
	{"cfb_LTX__is_block_algorithm_mode", cfb_LTX__is_block_algorithm_mode},
	{"cfb_LTX__mcrypt_get_modes_name", cfb_LTX__mcrypt_get_modes_name},
	{"cfb_LTX__mcrypt_mode_get_size", cfb_LTX__mcrypt_mode_get_size},
	{"cfb_LTX__mcrypt_mode_version", cfb_LTX__mcrypt_mode_version},
	{NULL, NULL}
};
